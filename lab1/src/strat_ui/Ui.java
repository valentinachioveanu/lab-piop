package strat_ui;

import calculator.Calculator;

import java.security.InvalidParameterException;
import java.util.Scanner;

public class Ui {
    private final Calculator calculator;
    private final Scanner scanner;

    public Ui() {
        calculator = new Calculator();
        scanner = new Scanner(System.in);
    }

    private String readOperation() {
        System.out.println("Operations: ");
        System.out.println("+ : addition");
        System.out.println("- : subtraction ");
        System.out.println("* : multiply");
        System.out.println("/ : division");
        System.out.print("Please select an operation: ");

        return scanner.next();
    }

    private double readNumber() {
        System.out.print("Please enter a number: ");

        return scanner.nextDouble();
    }

    public void start() {

        double number;
        number = readNumber();

        calculator.addition(number);
        while(true) {
            number = calculator.getLastResult();
            System.out.println("\nYour total: " + number);
            String operation = readOperation();

            number = readNumber();

            switch(operation) {
                case "+":
                    calculator.addition(number);
                    number = calculator.getLastResult();
                    System.out.println("Your total: " + number);
                    break;
                case "-":
                    calculator.subtraction(number);
                    number = calculator.getLastResult();
                    System.out.println("Your total: " + number);
                    break;
                case "*":
                    calculator.multiply(number);
                    number = calculator.getLastResult();
                    System.out.println("Your total: " + number);
                    break;
                case "/":
                    try {
                        calculator.division(number);
                        number = calculator.getLastResult();
                        System.out.println("Your total: " + number);
                    } catch (InvalidParameterException ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                default:
                    System.out.println("Operation is not supported.");
            }

            System.out.print("Do you want to continue?(Y/N) ");
            String response = scanner.next();
            if(response.equals("N")) {
                return;
            }

            System.out.print("Do you want to reset the calculator?(Y/N) ");
            response = scanner.next();
            if(response.equals("Y")) {
                calculator.resetCalculator();
                number = readNumber();
                calculator.addition(number);
            }
        }
    }
}
