package calculator;

import java.security.InvalidParameterException;

public class Calculator {
    private double lastResult;

    public Calculator() {
        lastResult = 0;
    }

    public double getLastResult() {
        return lastResult;
    }

    public void resetCalculator() {
        lastResult = 0;
    }

    public void addition(double number) {
        lastResult += number;
    }

    public void subtraction(double number) {
        lastResult -= number;
    }

    public void multiply(double number) {
        lastResult *= number;
    }

    public void division(double number) throws InvalidParameterException {
        if(number != 0)
            lastResult /= number;
        else
            throw new InvalidParameterException("Division by 0 is not allowed.");
    }
}
