package com.tora.domain;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class Order extends Entity<Integer> implements Comparable<Order> {
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        super(id);
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return getId() == order.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + getId() +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int compareTo(@NotNull Order o) {
        if(this.getId() < o.getId())
            return -1;
        return 1;
    }
}
