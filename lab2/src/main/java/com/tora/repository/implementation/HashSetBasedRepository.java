package com.tora.repository.implementation;

import com.tora.repository.InMemoryRepository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    Set<T> objects;

    public HashSetBasedRepository() {
        objects = new HashSet<>();
    }

    @Override
    public void add(T t) {
        objects.add(t);
    }

    @Override
    public boolean contains(T t) {
        return objects.contains(t);
    }

    @Override
    public void remove(T t) {
        objects.remove(t);
    }
}
