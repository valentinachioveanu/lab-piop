package com.tora.repository.implementation;

import com.tora.repository.InMemoryRepository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    Set<T> objects;

    public TreeSetBasedRepository() {
        objects = new TreeSet<>();
    }

    @Override
    public void add(T t) {
        objects.add(t);
    }

    @Override
    public boolean contains(T t) {
        return objects.contains(t);
    }

    @Override
    public void remove(T t) {
        objects.remove(t);
    }
}
