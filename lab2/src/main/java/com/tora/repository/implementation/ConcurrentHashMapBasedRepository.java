package com.tora.repository.implementation;

import com.tora.domain.Entity;
import com.tora.repository.InMemoryRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<K, T extends Entity<K>> implements InMemoryRepository<T> {
    Map<K, T> objects;

    public ConcurrentHashMapBasedRepository() {
        objects = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T t) {
        objects.put(t.getId(), t);
    }

    @Override
    public boolean contains(T t) {
        return objects.containsKey(t.getId());
    }

    @Override
    public void remove(T t) {
        objects.remove(t.getId());
    }
}
