package com.tora.repository.implementation;

import com.tora.repository.InMemoryRepository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    List<T> objects;

    public ArrayListBasedRepository() {
        objects = new ArrayList<>();
    }

    @Override
    public void add(T t) {
        objects.add(t);
    }

    @Override
    public boolean contains(T t) {
        return objects.contains(t);
    }

    @Override
    public void remove(T t) {
        objects.remove(t);
    }
}
