package com.tora.repository.implementation;

import com.koloboke.collect.set.hash.HashObjSet;
import com.koloboke.collect.set.hash.HashObjSets;
import com.tora.repository.InMemoryRepository;

public class KolobokeBasedRepository<T> implements InMemoryRepository<T> {
    HashObjSet<T> objects;

    public KolobokeBasedRepository() {
        objects = HashObjSets.newMutableSet();
    }

    @Override
    public void add(T t) {
        objects.add(t);
    }

    @Override
    public boolean contains(T t) {
        return objects.contains(t);
    }

    @Override
    public void remove(T t) {
        objects.remove(t);
    }
}
