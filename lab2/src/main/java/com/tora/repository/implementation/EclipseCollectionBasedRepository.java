package com.tora.repository.implementation;

import com.tora.repository.InMemoryRepository;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

import java.util.List;

public class EclipseCollectionBasedRepository<T> implements InMemoryRepository<T> {
    MutableList<T> objects ;

    public EclipseCollectionBasedRepository() {
        objects = Lists.mutable.empty();
    }

    @Override
    public void add(T t) {
        objects.add(t);
    }

    @Override
    public boolean contains(T t) {
        return objects.contains(t);
    }

    @Override
    public void remove(T t) {
        objects.remove(t);
    }
}
