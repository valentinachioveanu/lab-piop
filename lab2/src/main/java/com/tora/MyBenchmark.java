package com.tora;

import com.tora.domain.Order;
import com.tora.repository.implementation.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class MyBenchmark {
    private static Order order1 = new Order(1, 23, 55);
    private static Order order2 = new Order(2, 45, 22);
    private static Order order3 = new Order(3, 54, 46);
    private static Order orderToAdd = new Order(4, 43, 96);

    @State(Scope.Benchmark)
    public static class ArrayListState {
        ArrayListBasedRepository<Order> repository = new ArrayListBasedRepository<>();

        @Setup(Level.Trial)
        public void doSetup() {
            repository.add(order1);
            repository.add(order2);
            repository.add(order3);
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        HashSetBasedRepository<Order> repository = new HashSetBasedRepository<>();

        @Setup(Level.Trial)
        public void doSetup() {
            repository.add(order1);
            repository.add(order2);
            repository.add(order3);
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        TreeSetBasedRepository<Order> repository = new TreeSetBasedRepository<>();

        @Setup(Level.Trial)
        public void doSetup() {
            repository.add(order1);
            repository.add(order2);
            repository.add(order3);
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        ConcurrentHashMapBasedRepository<Integer, Order> repository = new ConcurrentHashMapBasedRepository<>();

        @Setup(Level.Trial)
        public void doSetup() {
            repository.add(order1);
            repository.add(order2);
            repository.add(order3);
        }
    }

    @State(Scope.Benchmark)
    public static class EclipseCollectionState {
        EclipseCollectionBasedRepository<Order> repository = new EclipseCollectionBasedRepository<>();

        @Setup(Level.Trial)
        public void doSetup() {
            repository.add(order1);
            repository.add(order2);
            repository.add(order3);
        }
    }

    @State(Scope.Benchmark)
    public static class KolobokeState {
        KolobokeBasedRepository<Order> repository = new KolobokeBasedRepository<>();

        @Setup(Level.Trial)
        public void doSetup() {
            repository.add(order1);
            repository.add(order2);
            repository.add(order3);
        }
    }

    @Benchmark
    public void testAddArrayList(ArrayListState state) {
        state.repository.add(orderToAdd);
    }

    @Benchmark
    public void testContainsArrayList(ArrayListState state) {
        state.repository.contains(order1);
    }

    @Benchmark
    public void testRemoveArrayList(ArrayListState state) {
        state.repository.remove(order2);
    }

    @Benchmark
    public void testAddHashSet(HashSetState state) {
        state.repository.add(orderToAdd);
    }

    @Benchmark
    public void testContainsHashSet(HashSetState state) {
        state.repository.contains(order1);
    }

    @Benchmark
    public void testRemoveHashSet(HashSetState state) {
        state.repository.remove(order2);
    }

    @Benchmark
    public void testAddTreeSet(TreeSetState state) {
        state.repository.add(orderToAdd);
    }

    @Benchmark
    public void testContainsTreeSet(TreeSetState state) {
        state.repository.contains(order1);
    }

    @Benchmark
    public void testRemoveTreeSet(TreeSetState state) {
        state.repository.remove(order2);
    }

    @Benchmark
    public void testAddConcurrentHashMap(ConcurrentHashMapState state) {
        state.repository.add(orderToAdd);
    }

    @Benchmark
    public void testContainsConcurrentHashMap(ConcurrentHashMapState state) {
        state.repository.contains(order1);
    }

    @Benchmark
    public void testRemoveConcurrentHashMap(ConcurrentHashMapState state) {
        state.repository.remove(order2);
    }

    @Benchmark
    public void testAddEclipseCollection(EclipseCollectionState state) {
        state.repository.add(orderToAdd);
    }

    @Benchmark
    public void testContainsEclipseCollection(EclipseCollectionState state) {
        state.repository.contains(order1);
    }

    @Benchmark
    public void testRemoveEclipseCollection(EclipseCollectionState state) {
        state.repository.remove(order2);
    }

    @Benchmark
    public void testAddKoloboke(EclipseCollectionState state) {
        state.repository.add(orderToAdd);
    }

    @Benchmark
    public void testContainsKoloboke(EclipseCollectionState state) {
        state.repository.contains(order1);
    }

    @Benchmark
    public void testRemoveKoloboke(EclipseCollectionState state) {
        state.repository.remove(order2);
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(options).run();
    }
}
