import com.tora.serialization.BigDecimalSerialized;
import com.tora.utils.Utils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BigDecimalTest {
    public List<BigDecimal> decimals;
    public List<BigDecimalSerialized> decimalsSerialized;

    @Before
    public void init() {
        this.decimals = new ArrayList<>(List.of(new BigDecimal("52.9"), new BigDecimal("89.1"),
                new BigDecimal("22.4"), new BigDecimal("37"), new BigDecimal("76.1"),
                new BigDecimal("41.7"), new BigDecimal("7"), new BigDecimal("19.8"),
                new BigDecimal("15.4"), new BigDecimal("33.7")));

        this.decimalsSerialized = Utils.convert(decimals);
    }

    @Test
    public void sum() {
        assertEquals(new BigDecimal("395.1"), Utils.sum(decimals));
    }

    @Test
    public void average() {
        double epsilon = 0.0000001d;
        assertEquals(39.51, Utils.average(decimals), epsilon);
    }

    @Test
    public void top() {
        assertEquals(1, Utils.top10(decimals).size());
        assertEquals(new BigDecimal("89.1"), Utils.top10(decimals).get(0));
    }

    @Test
    public void sumSerialized() {
        assertEquals(new BigDecimalSerialized(new BigDecimal("395.1")), Utils.sumSerialized(decimalsSerialized));
    }

    @Test
    public void averageSerialized() {
        double epsilon = 0.0000001d;
        assertEquals(39.51, Utils.averageSerialized(decimalsSerialized), epsilon);
    }

    @Test
    public void topSerialized() {
        assertEquals(1, Utils.top10Serialized(decimalsSerialized).size());
        assertEquals(new BigDecimalSerialized(new BigDecimal("89.1")), Utils.top10Serialized(decimalsSerialized).get(0));
    }
}
