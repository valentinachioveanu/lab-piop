package com.tora.utils;

import com.tora.serialization.BigDecimalSerialized;

import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Utils {
    public static BigDecimal sum(List<BigDecimal> decimals) {
        return decimals
                .stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static double average(List<BigDecimal> decimals) {
        return decimals
                .stream()
                .mapToDouble(BigDecimal::doubleValue)
                .average()
                .getAsDouble();
    }

    public static List<BigDecimal> top10(List<BigDecimal> decimals) {
        return decimals
                .stream()
                .sorted(Comparator.reverseOrder())
                .limit(decimals.size() / 10)
                .collect(Collectors.toList());
    }

    public static void serialize(BigDecimalSerialized decimal) throws IOException {
        FileOutputStream fileOut = new FileOutputStream("C:\\Users\\valen\\OneDrive\\Desktop\\fac\\sem5\\PIOP\\labs\\lab3\\file\\big-decimal-serialized.txt");
        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);

        objOut.writeObject(decimal);
        objOut.close();
    }

    public static BigDecimalSerialized deserialize() throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream("C:\\Users\\valen\\OneDrive\\Desktop\\fac\\sem5\\PIOP\\labs\\lab3\\file\\big-decimal-serialized.txt");
        ObjectInputStream objIn = new ObjectInputStream(fileIn);

        BigDecimalSerialized decimal = (BigDecimalSerialized) objIn.readObject();
        objIn.close();

        return decimal;
    }

    public static List<BigDecimalSerialized> convert(List<BigDecimal> decimals) {
        return decimals
                .stream()
                .map(decimal -> {
                    try {
                        serialize(new BigDecimalSerialized(decimal));
                        return deserialize();
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

    public static BigDecimalSerialized sumSerialized(List<BigDecimalSerialized> decimals) {
        return new BigDecimalSerialized(decimals
                .stream()
                .map(BigDecimalSerialized::getDecimal)
                .reduce(BigDecimal.ZERO, BigDecimal::add));
    }

    public static double averageSerialized(List<BigDecimalSerialized> decimals) {
        return decimals
                .stream()
                .map(BigDecimalSerialized::getDecimal)
                .mapToDouble(BigDecimal::doubleValue)
                .average()
                .getAsDouble();
    }

    public static List<BigDecimalSerialized> top10Serialized(List<BigDecimalSerialized> decimals) {
        return decimals
                .stream()
                .map(BigDecimalSerialized::getDecimal)
                .sorted(Comparator.reverseOrder())
                .limit(decimals.size() / 10)
                .map(BigDecimalSerialized::new)
                .collect(Collectors.toList());
    }

    public static List<BigDecimal> generate(int length) {
        List<BigDecimal> decimals = new ArrayList<>();
        int i = 0;
        while(i < length) {
            decimals.add(new BigDecimal(new Random().nextLong()));
            i++;
        }

        return decimals;
    }
}
