package com.tora;

import com.tora.serialization.BigDecimalSerialized;
import com.tora.utils.Utils;

import java.math.BigDecimal;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<BigDecimal> decimals = Utils.generate(100000);
        List<BigDecimalSerialized> decimalsSerialized = Utils.convert(decimals);

        System.out.print("Sum - decimals: ");
        System.out.print(Utils.sum(decimals));
        System.out.print("\n");

        System.out.print("Sum - decimals serialized: ");
        System.out.print(Utils.sumSerialized(decimalsSerialized).getDecimal());
        System.out.print("\n");

        System.out.print("Avg - decimals: ");
        System.out.print(Utils.average(decimals));
        System.out.print("\n");

        System.out.print("Avg - decimals serialized: ");
        System.out.print(Utils.averageSerialized(decimalsSerialized));
        System.out.print("\n");

        System.out.print("Top 10 - decimals: ");
        System.out.print(Utils.top10(decimals));
        System.out.print("\n");

        System.out.print("Top 10 - decimals serialized: ");
        System.out.print(Utils.top10Serialized(decimalsSerialized));
        System.out.print("\n");
    }
}