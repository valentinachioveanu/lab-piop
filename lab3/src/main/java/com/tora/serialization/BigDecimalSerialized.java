package com.tora.serialization;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.math.BigDecimal;

public class BigDecimalSerialized implements Externalizable {
    private BigDecimal decimal;

    public BigDecimalSerialized(){}

    public BigDecimalSerialized(BigDecimal decimal) {
        this.decimal = decimal;
    }

    public BigDecimal getDecimal() {
        return decimal;
    }

    public void setDecimal(BigDecimal decimal) {
        this.decimal = decimal;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(decimal);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.decimal = (BigDecimal) in.readObject();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.equals(decimal);
    }
}
